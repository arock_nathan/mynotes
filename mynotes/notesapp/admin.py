from django.contrib import admin
from notesapp import models

# Register your models here.
admin.site.register(models.tags)
admin.site.register(models.mynotes)
admin.site.register(models.note_tags)