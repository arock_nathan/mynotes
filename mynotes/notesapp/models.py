from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class mynotes(models.Model):
    note_id = models.AutoField(db_column='note_id', primary_key=True)
    note_text = models.TextField(db_column='note_text', max_length=2000, null=False, blank=False)
    added_date = models.DateTimeField(auto_now_add=True)
    status = models.TextField(max_length=10,null=False,blank=False)

class tags(models.Model):
    tag_id = models.AutoField(db_column='tag_id', primary_key=True)
    tag_name = models.TextField(db_column='tag_name', null=False, blank=False)
    added_date = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.tag_name
    

class note_tags(models.Model):
    note_tag_id = models.AutoField(db_column='note_tag_id', primary_key=True)
    note_id = models.ForeignKey(mynotes, on_delete=models.CASCADE)
    tag_id = models.ForeignKey(tags, on_delete=models.CASCADE)
