from django.shortcuts import render, HttpResponse

# Create your views here.

def index(request):
    return render(request, template_name='notes_list.html')