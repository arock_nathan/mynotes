from django.contrib import admin
from django.urls import path, include
from notesapp import views


urlpatterns = [
    path(r'list', views.index, name='list_notes'),
]
